import { shallowMount, createLocalVue } from "@vue/test-utils";
import BootstrapVue from "bootstrap-vue";
import JobDetails from "@/components/JobDetails";

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe("JobDetails.vue", () => {
	it("renders without errors", () => {
		const $route = {
			path: "/viewJob/:id",
			params: {
				id: 40
			}
		};
		const $store = {
			dispatch() {
				return {
					getItems() {
						return [];
					}
				};
			},
			state: {
				items: []
			},
			getters: {
				getJobDetails() {}
			}
		};
		shallowMount(JobDetails, {
			mocks: {
				$route,
				$store
			},
			localVue
		});
	});
});
