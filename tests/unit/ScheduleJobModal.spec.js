import { expect } from "chai";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import ScheduleJobModal from "@/components/ScheduleJobModal";
import BootstrapVue from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe("ScheduleJobModal.vue", () => {
	it("renders without errors", () => {
		const wrapper = shallowMount(ScheduleJobModal, {
			propsData: {
				jobId: 2,
				jobName: "Omkar"
			},
			computed: {
				id() {
					return 12;
				},
				name() {
					return "Job 12";
				}
			},
			localVue
		});
		expect(wrapper.text()).to.include("Job 12");
	});
});
