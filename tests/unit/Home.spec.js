import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Home from "@/components/Home";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import BootstrapVue from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(BootstrapVue);
localVue.component("font-awesome-icon", FontAwesomeIcon);

describe("Home.vue", () => {
	it("renders without errors", () => {
		shallowMount(Home, {
			mocks: {
				$store: {
					state: {
						items: []
					},
					dispatch() {
						return {
							getItems() {
								return [];
							}
						};
					}
				}
			},
			localVue
		});
	});
});
