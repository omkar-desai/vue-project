import { expect } from "chai";
import Vuex from "vuex";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import JobList from "@/components/JobList";
import BootstrapVue from "bootstrap-vue";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

const localVue = createLocalVue();
localVue.use(Vuex);
localVue.use(BootstrapVue);
localVue.component("font-awesome-icon", FontAwesomeIcon);

describe("JobList.vue", () => {
	it("renders without errors", () => {
		const wrapper = shallowMount(JobList, {
			stubs: ["router-link"],
			propsData: {
				items: []
			},
			mocks: {
				$store: {
					state: {
						items: [],
						jobListError: false,
						noJobList: true
					},
					getters: {}
				}
			},
			localVue
		});
		expect(wrapper.text()).to.include("No Data Available");
	});
});
