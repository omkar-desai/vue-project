import { expect } from "chai";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import BootstrapVue from "bootstrap-vue";
import Error404 from "@/components/Error404";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";

const localVue = createLocalVue();
localVue.component("font-awesome-icon", FontAwesomeIcon);
localVue.use(BootstrapVue);

describe("Error404.vue", () => {
	it("renders without errors", () => {
		const wrapper = shallowMount(Error404, {
			stubs: ["router-link"],
			localVue
		});
		expect(wrapper.text()).to.include("We're sorry.");
	});
});
