import { expect } from "chai";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import Spinner from "@/components/Spinner";
import BootstrapVue from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe("Spinner.vue", () => {
	it("renders without errors", () => {
		const wrapper = shallowMount(Spinner, { localVue });
		expect(wrapper.text()).to.include("Loading");
	});
});
