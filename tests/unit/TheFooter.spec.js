import { expect } from "chai";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import TheFooter from "@/components/TheFooter";
import BootstrapVue from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe("TheFooter.vue", () => {
	it("renders without errors", () => {
		const wrapper = shallowMount(TheFooter, {
			localVue,
			propsData: {
				year: 2020
			}
		});
		expect(wrapper.text()).to.include("2020");
	});
});
