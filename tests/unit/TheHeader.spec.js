import { expect } from "chai";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import TheHeader from "@/components/TheHeader";
import BootstrapVue from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe("TheHeader.vue", () => {
	it("renders without errors", () => {
		const wrapper = shallowMount(TheHeader, { localVue });
		expect(wrapper.text()).to.include("Job Framework");
	});
});
