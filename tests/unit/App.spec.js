import { shallowMount, createLocalVue } from "@vue/test-utils";
import Vuex from "vuex";
import VueRouter from "vue-router";
import App from "@/App";
import BootstrapVue from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(VueRouter);
localVue.use(Vuex);
localVue.use(BootstrapVue);

const store = new Vuex.Store({
	state: {
		items: []
	},
	getters: {},
	dispatch() {
		return {
			getItems() {
				return [];
			}
		};
	}
});
const router = new VueRouter();

describe("App.vue", () => {
	it("renders without errors", () => {
		shallowMount(App, {
			router,
			store,
			localVue
		});
	});
});
