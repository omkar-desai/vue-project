import { expect } from "chai";
import { shallowMount, createLocalVue } from "@vue/test-utils";
import SubHeader from "@/components/SubHeader";
import BootstrapVue from "bootstrap-vue";

const localVue = createLocalVue();
localVue.use(BootstrapVue);

describe("SubHeader.vue", () => {
	it("renders without errors", () => {
		const wrapper = shallowMount(SubHeader, {
			propsData: {
				headerText: "Header"
			},
			localVue
		});
		expect(wrapper.text()).to.include("Header");
	});
});
