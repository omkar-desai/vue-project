import Vue from "vue";
import VueRouter from "vue-router";
import store from "./store";
import routes from "./routes";
import App from "./App.vue";
import BootstrapVue from "bootstrap-vue";
import { library } from "@fortawesome/fontawesome-svg-core";
import {
	faBan,
	faClock,
	faAngleLeft,
	faLongArrowAltLeft,
	faExclamationTriangle,
	faExclamationCircle,
	faCog,
	faCheck,
	faExclamation
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/vue-fontawesome";
import "./index.scss";
import "./registerServiceWorker";

library.add(
	faBan,
	faClock,
	faAngleLeft,
	faLongArrowAltLeft,
	faExclamationTriangle,
	faExclamationCircle,
	faCog,
	faCheck,
	faExclamation
);

Vue.component("font-awesome-icon", FontAwesomeIcon);
Vue.use(BootstrapVue);
Vue.use(VueRouter);

Vue.config.productionTip = false;

const router = new VueRouter({
	routes,
	mode: "history"
});

new Vue({
	store,
	router,
	render: h => h(App)
}).$mount("#app");
