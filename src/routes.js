const Home = () => import("./components/Home");
const JobDetails = () => import("./components/JobDetails");
const Error404 = () => import("./components/Error404");

export default [
	{
		path: "/",
		component: Home
	},
	{
		path: "/home",
		component: Home
	},
	{
		path: "/viewJob/:id",
		component: JobDetails
	},
	{ path: "/404", component: Error404, name: "error" },
	{ path: "*", component: Error404 }
];
