import Vue from "vue";
import Vuex from "vuex";
import axios from "axios";

Vue.use(Vuex);

export default new Vuex.Store({
	state: { items: [], jobListError: false, noJobList: false },
	getters: {
		getJobDetails: state => id =>
			state.items.find(item => item.jobId == id),
		isLoaded: state => !!state.items.length
	},
	mutations: {
		setJobListError(state, val) {
			state.jobListError = val;
		},
		setNoJobList(state, val) {
			state.noJobList = val;
		},
		setItems(state, items) {
			state.items = items;
		},
		updateItem(state, item) {
			const arrIndex = state.items.findIndex(elem => {
				return item.jobId == elem.jobId;
			});
			Vue.set(state.items, arrIndex, item);
		},
		removeItem(state, id) {
			const arrIndex = state.items.findIndex(elem => {
				return id == elem.id;
			});
			state.items.splice(arrIndex, 1);
		}
	},
	actions: {
		getItems(context) {
			axios
				.get("http://192.168.81.54:8080/job-scehedular/getdashboard")
				.then(
					res => {
						const items = res.data;
						context.commit("setJobListError", false);
						if (items.length) {
							context.commit("setItems", items);
							context.commit("setNoJobList", false);
						} else {
							context.commit("setNoJobList", true);
						}
					},
					() => {
						context.commit("setItems", []);
						context.commit("setJobListError", true);
					}
				);
		},
		scheduleItem(context, obj) {
			axios
				.post(
					"http://192.168.81.54:8080/job-scehedular/schedule-job",
					obj.item
				)
				.then(
					res => {
						context.commit("updateItem", res.data);
						obj.vm.$bvToast.toast(
							`${obj.item.jobName} scheduled successfully.`,
							{
								title: "Success",
								variant: "success",
								solid: true
							}
						);
					},
					res => {
						obj.vm.$bvToast.toast(
							res.response &&
								res.response.data &&
								res.response.data.message
								? `${obj.item.jobName} scheduling failed. ${res.response.data.message}`
								: `${obj.item.jobName} scheduling failed. Please try again later.`,
							{
								title: "Failed",
								variant: "danger",
								solid: true
							}
						);
					}
				);
		},
		removeItem(context, jobName) {
			axios
				.post(
					"http://192.168.81.54:8080/job-scehedular/unschedule-job",
					{
						jobName
					}
				)
				.then(
					res => {
						console.log(res);
						context.commit("removeItem", jobName);
					},
					() => {}
				);
		}
	}
});
